// @SOURCE:E:/Gamealoon/gamealoon/conf/routes
// @HASH:f4895ae97d14f719adacc65149ef744e041a4453
// @DATE:Sat Dec 14 19:29:23 IST 2013

package com.gamealoon.controllers;

public class routes {
public static final com.gamealoon.controllers.ReverseGameController GameController = new com.gamealoon.controllers.ReverseGameController();
public static final com.gamealoon.controllers.ReverseArticleController ArticleController = new com.gamealoon.controllers.ReverseArticleController();
public static final com.gamealoon.controllers.ReverseAdminController AdminController = new com.gamealoon.controllers.ReverseAdminController();
public static final com.gamealoon.controllers.ReversePlatformController PlatformController = new com.gamealoon.controllers.ReversePlatformController();
public static final com.gamealoon.controllers.ReverseMediaController MediaController = new com.gamealoon.controllers.ReverseMediaController();
public static final com.gamealoon.controllers.ReverseUserController UserController = new com.gamealoon.controllers.ReverseUserController();
public static final com.gamealoon.controllers.ReverseAchievementController AchievementController = new com.gamealoon.controllers.ReverseAchievementController();
public static final com.gamealoon.controllers.ReverseSearchController SearchController = new com.gamealoon.controllers.ReverseSearchController();
public static final com.gamealoon.controllers.ReverseConversationController ConversationController = new com.gamealoon.controllers.ReverseConversationController();
public static class javascript {
public static final com.gamealoon.controllers.javascript.ReverseGameController GameController = new com.gamealoon.controllers.javascript.ReverseGameController();
public static final com.gamealoon.controllers.javascript.ReverseArticleController ArticleController = new com.gamealoon.controllers.javascript.ReverseArticleController();
public static final com.gamealoon.controllers.javascript.ReverseAdminController AdminController = new com.gamealoon.controllers.javascript.ReverseAdminController();
public static final com.gamealoon.controllers.javascript.ReversePlatformController PlatformController = new com.gamealoon.controllers.javascript.ReversePlatformController();
public static final com.gamealoon.controllers.javascript.ReverseMediaController MediaController = new com.gamealoon.controllers.javascript.ReverseMediaController();
public static final com.gamealoon.controllers.javascript.ReverseUserController UserController = new com.gamealoon.controllers.javascript.ReverseUserController();
public static final com.gamealoon.controllers.javascript.ReverseAchievementController AchievementController = new com.gamealoon.controllers.javascript.ReverseAchievementController();
public static final com.gamealoon.controllers.javascript.ReverseSearchController SearchController = new com.gamealoon.controllers.javascript.ReverseSearchController();
public static final com.gamealoon.controllers.javascript.ReverseConversationController ConversationController = new com.gamealoon.controllers.javascript.ReverseConversationController();
}
public static class ref {
public static final com.gamealoon.controllers.ref.ReverseGameController GameController = new com.gamealoon.controllers.ref.ReverseGameController();
public static final com.gamealoon.controllers.ref.ReverseArticleController ArticleController = new com.gamealoon.controllers.ref.ReverseArticleController();
public static final com.gamealoon.controllers.ref.ReverseAdminController AdminController = new com.gamealoon.controllers.ref.ReverseAdminController();
public static final com.gamealoon.controllers.ref.ReversePlatformController PlatformController = new com.gamealoon.controllers.ref.ReversePlatformController();
public static final com.gamealoon.controllers.ref.ReverseMediaController MediaController = new com.gamealoon.controllers.ref.ReverseMediaController();
public static final com.gamealoon.controllers.ref.ReverseUserController UserController = new com.gamealoon.controllers.ref.ReverseUserController();
public static final com.gamealoon.controllers.ref.ReverseAchievementController AchievementController = new com.gamealoon.controllers.ref.ReverseAchievementController();
public static final com.gamealoon.controllers.ref.ReverseSearchController SearchController = new com.gamealoon.controllers.ref.ReverseSearchController();
public static final com.gamealoon.controllers.ref.ReverseConversationController ConversationController = new com.gamealoon.controllers.ref.ReverseConversationController();
}
}
          
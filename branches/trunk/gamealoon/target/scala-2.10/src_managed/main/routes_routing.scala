// @SOURCE:E:/Gamealoon/gamealoon/conf/routes
// @HASH:f4895ae97d14f719adacc65149ef744e041a4453
// @DATE:Sat Dec 14 19:29:23 IST 2013


import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString

object Routes extends Router.Routes {

private var _prefix = "/"

def setPrefix(prefix: String) {
  _prefix = prefix
  List[(String,Routes)]().foreach {
    case (p, router) => router.setPrefix(prefix + (if(prefix.endsWith("/")) "" else "/") + p)
  }
}

def prefix = _prefix

lazy val defaultPrefix = { if(Routes.prefix.endsWith("/")) "" else "/" }


// @LINE:7
private[this] lazy val com_gamealoon_controllers_PlatformController_getPlatformData0 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("platform/"),DynamicPart("platform", """[^/]+""",true),StaticPart("/"),DynamicPart("category", """[^/]+""",true))))
        

// @LINE:11
private[this] lazy val com_gamealoon_controllers_AdminController_getAdminData1 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/"))))
        

// @LINE:12
private[this] lazy val com_gamealoon_controllers_GameController_createOrUpdateGame2 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/game/save"))))
        

// @LINE:13
private[this] lazy val com_gamealoon_controllers_GameController_getNGames3 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/games/"),DynamicPart("timestamp", """[^/]+""",true))))
        

// @LINE:14
private[this] lazy val com_gamealoon_controllers_AchievementController_createOrUpdateAchievement4 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/achievement/save"))))
        

// @LINE:15
private[this] lazy val com_gamealoon_controllers_AchievementController_getNAchievements5 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/achievements/"),DynamicPart("timestamp", """[^/]+""",true))))
        

// @LINE:16
private[this] lazy val com_gamealoon_controllers_PlatformController_createOrUpdatePlatform6 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/platform/save"))))
        

// @LINE:17
private[this] lazy val com_gamealoon_controllers_PlatformController_getNPlatforms7 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("admin/platforms/"),DynamicPart("timestamp", """[^/]+""",true))))
        

// @LINE:22
private[this] lazy val com_gamealoon_controllers_ArticleController_getArticle8 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("article/"),DynamicPart("username", """[^/]+""",true),StaticPart("/"),DynamicPart("titleOrId", """[^/]+""",true))))
        

// @LINE:23
private[this] lazy val com_gamealoon_controllers_ArticleController_getNArticlesByCarouselSelectorAndCategory9 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("articles/"),DynamicPart("carouselSelector", """[^/]+""",true),StaticPart("/"),DynamicPart("category", """[^/]+""",true),StaticPart("/"),DynamicPart("timestamp", """[^/]+""",true),StaticPart("/"),DynamicPart("mode", """[^/]+""",true))))
        

// @LINE:24
private[this] lazy val com_gamealoon_controllers_ArticleController_saveOrUpdateArticle10 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("article/save"))))
        

// @LINE:25
private[this] lazy val com_gamealoon_controllers_ArticleController_createOrUpdateCoolOrNotCoolState11 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("article/votingStateUpdate"))))
        

// @LINE:26
private[this] lazy val com_gamealoon_controllers_ArticleController_updateAverageTimeSpent12 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("article/updateAverageTimeSpent"))))
        

// @LINE:31
private[this] lazy val com_gamealoon_controllers_UserController_registerUser13 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/register"))))
        

// @LINE:32
private[this] lazy val com_gamealoon_controllers_UserController_getLoggedInUser14 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("login"))))
        

// @LINE:33
private[this] lazy val com_gamealoon_controllers_UserController_getUser15 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/"),DynamicPart("usernameOrId", """[^/]+""",true),StaticPart("/"),DynamicPart("mode", """[^/]+""",true),StaticPart("/"),DynamicPart("username", """[^/]+""",true))))
        

// @LINE:34
private[this] lazy val com_gamealoon_controllers_UserController_saveOrUpdateUserInterest16 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/interest/"),DynamicPart("userName", """[^/]+""",true))))
        

// @LINE:35
private[this] lazy val com_gamealoon_controllers_UserController_saveOrUpdateUser17 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/save/"),DynamicPart("userName", """[^/]+""",true))))
        

// @LINE:36
private[this] lazy val com_gamealoon_controllers_UserController_resetPassword18 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/password/change/"),DynamicPart("userName", """[^/]+""",true))))
        

// @LINE:37
private[this] lazy val com_gamealoon_controllers_UserController_addOrRemoveBuddy19 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/addOrRemoveBuddy"))))
        

// @LINE:38
private[this] lazy val com_gamealoon_controllers_UserController_blockOrUnblockBuddy20 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/blockOrUnblockBuddy"))))
        

// @LINE:39
private[this] lazy val com_gamealoon_controllers_UserController_addOrRemoveInterestedGames21 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/interestedOrNotInGame"))))
        

// @LINE:40
private[this] lazy val com_gamealoon_controllers_UserController_checkStatus22 = Route("OPTIONS", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/saveOrUpdateUserAvatar/"),DynamicPart("userName", """[^/]+""",true),StaticPart("/"),DynamicPart("mediaId", """[^/]+""",true))))
        

// @LINE:41
private[this] lazy val com_gamealoon_controllers_UserController_saveOrUpdateUserAvatar23 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/saveOrUpdateUserAvatar/"),DynamicPart("userName", """[^/]+""",true),StaticPart("/"),DynamicPart("mediaId", """[^/]+""",true))))
        

// @LINE:42
private[this] lazy val com_gamealoon_controllers_UserController_validateEmail24 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/validateEmail/"),DynamicPart("email", """[^/]+""",true))))
        

// @LINE:43
private[this] lazy val com_gamealoon_controllers_UserController_validateUsername25 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("user/validateUsername/"),DynamicPart("username", """[^/]+""",true))))
        

// @LINE:44
private[this] lazy val com_gamealoon_controllers_UserController_getAllUsers26 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("users"))))
        

// @LINE:48
private[this] lazy val com_gamealoon_controllers_MediaController_checkStatus27 = Route("OPTIONS", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("media/uploadImage/"),DynamicPart("userName", """[^/]+""",true),StaticPart("/"),DynamicPart("mediaId", """[^/]+""",true),StaticPart("/"),DynamicPart("mediaOwnerType", """[^/]+""",true))))
        

// @LINE:49
private[this] lazy val com_gamealoon_controllers_MediaController_uploadImage28 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("media/uploadImage/"),DynamicPart("userName", """[^/]+""",true),StaticPart("/"),DynamicPart("mediaId", """[^/]+""",true),StaticPart("/"),DynamicPart("mediaOwnerType", """[^/]+""",true))))
        

// @LINE:50
private[this] lazy val com_gamealoon_controllers_MediaController_fetchImages29 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("media/images/"),DynamicPart("userName", """[^/]+""",true),StaticPart("/"),DynamicPart("timeStamp", """[^/]+""",true))))
        

// @LINE:54
private[this] lazy val com_gamealoon_controllers_SearchController_searchResponse30 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("search/"),DynamicPart("keywords", """[^/]+""",true))))
        

// @LINE:57
private[this] lazy val com_gamealoon_controllers_GameController_getGamesByTerm31 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("game/fetchGame/games/"),DynamicPart("term", """[^/]+""",true))))
        

// @LINE:58
private[this] lazy val com_gamealoon_controllers_GameController_getGame32 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("game/"),DynamicPart("urlOrid", """[^/]+""",true),StaticPart("/"),DynamicPart("username", """[^/]+""",true))))
        

// @LINE:59
private[this] lazy val com_gamealoon_controllers_GameController_getAllGames33 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("games"))))
        

// @LINE:66
private[this] lazy val com_gamealoon_controllers_ConversationController_addComment34 = Route("POST", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("converse/comment/save"))))
        

// @LINE:67
private[this] lazy val com_gamealoon_controllers_ConversationController_getComment35 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("converse/comment/get/"),DynamicPart("articleId", """[^/]+""",true),StaticPart("/"),DynamicPart("timeStamp", """[^/]+""",true))))
        

// @LINE:73
private[this] lazy val controllers_Assets_at36 = Route("GET", PathPattern(List(StaticPart(Routes.prefix),StaticPart(Routes.defaultPrefix),StaticPart("assets/"),DynamicPart("file", """.+""",false))))
        
def documentation = List(("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """platform/$platform<[^/]+>/$category<[^/]+>""","""com.gamealoon.controllers.PlatformController.getPlatformData(platform:String, category:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/""","""com.gamealoon.controllers.AdminController.getAdminData()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/game/save""","""com.gamealoon.controllers.GameController.createOrUpdateGame()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/games/$timestamp<[^/]+>""","""com.gamealoon.controllers.GameController.getNGames(timestamp:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/achievement/save""","""com.gamealoon.controllers.AchievementController.createOrUpdateAchievement()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/achievements/$timestamp<[^/]+>""","""com.gamealoon.controllers.AchievementController.getNAchievements(timestamp:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/platform/save""","""com.gamealoon.controllers.PlatformController.createOrUpdatePlatform()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """admin/platforms/$timestamp<[^/]+>""","""com.gamealoon.controllers.PlatformController.getNPlatforms(timestamp:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """article/$username<[^/]+>/$titleOrId<[^/]+>""","""com.gamealoon.controllers.ArticleController.getArticle(username:String, titleOrId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """articles/$carouselSelector<[^/]+>/$category<[^/]+>/$timestamp<[^/]+>/$mode<[^/]+>""","""com.gamealoon.controllers.ArticleController.getNArticlesByCarouselSelectorAndCategory(carouselSelector:String, category:String, timestamp:String, mode:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """article/save""","""com.gamealoon.controllers.ArticleController.saveOrUpdateArticle()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """article/votingStateUpdate""","""com.gamealoon.controllers.ArticleController.createOrUpdateCoolOrNotCoolState()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """article/updateAverageTimeSpent""","""com.gamealoon.controllers.ArticleController.updateAverageTimeSpent()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/register""","""com.gamealoon.controllers.UserController.registerUser()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """login""","""com.gamealoon.controllers.UserController.getLoggedInUser()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/$usernameOrId<[^/]+>/$mode<[^/]+>/$username<[^/]+>""","""com.gamealoon.controllers.UserController.getUser(usernameOrId:String, mode:Integer, username:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/interest/$userName<[^/]+>""","""com.gamealoon.controllers.UserController.saveOrUpdateUserInterest(userName:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/save/$userName<[^/]+>""","""com.gamealoon.controllers.UserController.saveOrUpdateUser(userName:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/password/change/$userName<[^/]+>""","""com.gamealoon.controllers.UserController.resetPassword(userName:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/addOrRemoveBuddy""","""com.gamealoon.controllers.UserController.addOrRemoveBuddy()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/blockOrUnblockBuddy""","""com.gamealoon.controllers.UserController.blockOrUnblockBuddy()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/interestedOrNotInGame""","""com.gamealoon.controllers.UserController.addOrRemoveInterestedGames()"""),("""OPTIONS""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/saveOrUpdateUserAvatar/$userName<[^/]+>/$mediaId<[^/]+>""","""com.gamealoon.controllers.UserController.checkStatus(userName:String, mediaId:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/saveOrUpdateUserAvatar/$userName<[^/]+>/$mediaId<[^/]+>""","""com.gamealoon.controllers.UserController.saveOrUpdateUserAvatar(userName:String, mediaId:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/validateEmail/$email<[^/]+>""","""com.gamealoon.controllers.UserController.validateEmail(email:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """user/validateUsername/$username<[^/]+>""","""com.gamealoon.controllers.UserController.validateUsername(username:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """users""","""com.gamealoon.controllers.UserController.getAllUsers()"""),("""OPTIONS""", prefix + (if(prefix.endsWith("/")) "" else "/") + """media/uploadImage/$userName<[^/]+>/$mediaId<[^/]+>/$mediaOwnerType<[^/]+>""","""com.gamealoon.controllers.MediaController.checkStatus(userName:String, mediaId:String, mediaOwnerType:String)"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """media/uploadImage/$userName<[^/]+>/$mediaId<[^/]+>/$mediaOwnerType<[^/]+>""","""com.gamealoon.controllers.MediaController.uploadImage(userName:String, mediaId:String, mediaOwnerType:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """media/images/$userName<[^/]+>/$timeStamp<[^/]+>""","""com.gamealoon.controllers.MediaController.fetchImages(userName:String, timeStamp:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """search/$keywords<[^/]+>""","""com.gamealoon.controllers.SearchController.searchResponse(keywords:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """game/fetchGame/games/$term<[^/]+>""","""com.gamealoon.controllers.GameController.getGamesByTerm(term:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """game/$urlOrid<[^/]+>/$username<[^/]+>""","""com.gamealoon.controllers.GameController.getGame(urlOrid:String, username:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """games""","""com.gamealoon.controllers.GameController.getAllGames()"""),("""POST""", prefix + (if(prefix.endsWith("/")) "" else "/") + """converse/comment/save""","""com.gamealoon.controllers.ConversationController.addComment()"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """converse/comment/get/$articleId<[^/]+>/$timeStamp<[^/]+>""","""com.gamealoon.controllers.ConversationController.getComment(articleId:String, timeStamp:String)"""),("""GET""", prefix + (if(prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""","""controllers.Assets.at(path:String = "/public", file:String)""")).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
  case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
  case l => s ++ l.asInstanceOf[List[(String,String,String)]] 
}}
      

def routes:PartialFunction[RequestHeader,Handler] = {

// @LINE:7
case com_gamealoon_controllers_PlatformController_getPlatformData0(params) => {
   call(params.fromPath[String]("platform", None), params.fromPath[String]("category", None)) { (platform, category) =>
        invokeHandler(com.gamealoon.controllers.PlatformController.getPlatformData(platform, category), HandlerDef(this, "com.gamealoon.controllers.PlatformController", "getPlatformData", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """platform/$platform<[^/]+>/$category<[^/]+>"""))
   }
}
        

// @LINE:11
case com_gamealoon_controllers_AdminController_getAdminData1(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.AdminController.getAdminData(), HandlerDef(this, "com.gamealoon.controllers.AdminController", "getAdminData", Nil,"GET", """""", Routes.prefix + """admin/"""))
   }
}
        

// @LINE:12
case com_gamealoon_controllers_GameController_createOrUpdateGame2(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.GameController.createOrUpdateGame(), HandlerDef(this, "com.gamealoon.controllers.GameController", "createOrUpdateGame", Nil,"POST", """""", Routes.prefix + """admin/game/save"""))
   }
}
        

// @LINE:13
case com_gamealoon_controllers_GameController_getNGames3(params) => {
   call(params.fromPath[String]("timestamp", None)) { (timestamp) =>
        invokeHandler(com.gamealoon.controllers.GameController.getNGames(timestamp), HandlerDef(this, "com.gamealoon.controllers.GameController", "getNGames", Seq(classOf[String]),"GET", """""", Routes.prefix + """admin/games/$timestamp<[^/]+>"""))
   }
}
        

// @LINE:14
case com_gamealoon_controllers_AchievementController_createOrUpdateAchievement4(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.AchievementController.createOrUpdateAchievement(), HandlerDef(this, "com.gamealoon.controllers.AchievementController", "createOrUpdateAchievement", Nil,"POST", """""", Routes.prefix + """admin/achievement/save"""))
   }
}
        

// @LINE:15
case com_gamealoon_controllers_AchievementController_getNAchievements5(params) => {
   call(params.fromPath[String]("timestamp", None)) { (timestamp) =>
        invokeHandler(com.gamealoon.controllers.AchievementController.getNAchievements(timestamp), HandlerDef(this, "com.gamealoon.controllers.AchievementController", "getNAchievements", Seq(classOf[String]),"GET", """""", Routes.prefix + """admin/achievements/$timestamp<[^/]+>"""))
   }
}
        

// @LINE:16
case com_gamealoon_controllers_PlatformController_createOrUpdatePlatform6(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.PlatformController.createOrUpdatePlatform(), HandlerDef(this, "com.gamealoon.controllers.PlatformController", "createOrUpdatePlatform", Nil,"POST", """""", Routes.prefix + """admin/platform/save"""))
   }
}
        

// @LINE:17
case com_gamealoon_controllers_PlatformController_getNPlatforms7(params) => {
   call(params.fromPath[String]("timestamp", None)) { (timestamp) =>
        invokeHandler(com.gamealoon.controllers.PlatformController.getNPlatforms(timestamp), HandlerDef(this, "com.gamealoon.controllers.PlatformController", "getNPlatforms", Seq(classOf[String]),"GET", """""", Routes.prefix + """admin/platforms/$timestamp<[^/]+>"""))
   }
}
        

// @LINE:22
case com_gamealoon_controllers_ArticleController_getArticle8(params) => {
   call(params.fromPath[String]("username", None), params.fromPath[String]("titleOrId", None)) { (username, titleOrId) =>
        invokeHandler(com.gamealoon.controllers.ArticleController.getArticle(username, titleOrId), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "getArticle", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """article/$username<[^/]+>/$titleOrId<[^/]+>"""))
   }
}
        

// @LINE:23
case com_gamealoon_controllers_ArticleController_getNArticlesByCarouselSelectorAndCategory9(params) => {
   call(params.fromPath[String]("carouselSelector", None), params.fromPath[String]("category", None), params.fromPath[String]("timestamp", None), params.fromPath[String]("mode", None)) { (carouselSelector, category, timestamp, mode) =>
        invokeHandler(com.gamealoon.controllers.ArticleController.getNArticlesByCarouselSelectorAndCategory(carouselSelector, category, timestamp, mode), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "getNArticlesByCarouselSelectorAndCategory", Seq(classOf[String], classOf[String], classOf[String], classOf[String]),"GET", """""", Routes.prefix + """articles/$carouselSelector<[^/]+>/$category<[^/]+>/$timestamp<[^/]+>/$mode<[^/]+>"""))
   }
}
        

// @LINE:24
case com_gamealoon_controllers_ArticleController_saveOrUpdateArticle10(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.ArticleController.saveOrUpdateArticle(), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "saveOrUpdateArticle", Nil,"POST", """""", Routes.prefix + """article/save"""))
   }
}
        

// @LINE:25
case com_gamealoon_controllers_ArticleController_createOrUpdateCoolOrNotCoolState11(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.ArticleController.createOrUpdateCoolOrNotCoolState(), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "createOrUpdateCoolOrNotCoolState", Nil,"POST", """""", Routes.prefix + """article/votingStateUpdate"""))
   }
}
        

// @LINE:26
case com_gamealoon_controllers_ArticleController_updateAverageTimeSpent12(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.ArticleController.updateAverageTimeSpent(), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "updateAverageTimeSpent", Nil,"POST", """""", Routes.prefix + """article/updateAverageTimeSpent"""))
   }
}
        

// @LINE:31
case com_gamealoon_controllers_UserController_registerUser13(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.UserController.registerUser(), HandlerDef(this, "com.gamealoon.controllers.UserController", "registerUser", Nil,"POST", """""", Routes.prefix + """user/register"""))
   }
}
        

// @LINE:32
case com_gamealoon_controllers_UserController_getLoggedInUser14(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.UserController.getLoggedInUser(), HandlerDef(this, "com.gamealoon.controllers.UserController", "getLoggedInUser", Nil,"POST", """""", Routes.prefix + """login"""))
   }
}
        

// @LINE:33
case com_gamealoon_controllers_UserController_getUser15(params) => {
   call(params.fromPath[String]("usernameOrId", None), params.fromPath[Integer]("mode", None), params.fromPath[String]("username", None)) { (usernameOrId, mode, username) =>
        invokeHandler(com.gamealoon.controllers.UserController.getUser(usernameOrId, mode, username), HandlerDef(this, "com.gamealoon.controllers.UserController", "getUser", Seq(classOf[String], classOf[Integer], classOf[String]),"GET", """""", Routes.prefix + """user/$usernameOrId<[^/]+>/$mode<[^/]+>/$username<[^/]+>"""))
   }
}
        

// @LINE:34
case com_gamealoon_controllers_UserController_saveOrUpdateUserInterest16(params) => {
   call(params.fromPath[String]("userName", None)) { (userName) =>
        invokeHandler(com.gamealoon.controllers.UserController.saveOrUpdateUserInterest(userName), HandlerDef(this, "com.gamealoon.controllers.UserController", "saveOrUpdateUserInterest", Seq(classOf[String]),"POST", """""", Routes.prefix + """user/interest/$userName<[^/]+>"""))
   }
}
        

// @LINE:35
case com_gamealoon_controllers_UserController_saveOrUpdateUser17(params) => {
   call(params.fromPath[String]("userName", None)) { (userName) =>
        invokeHandler(com.gamealoon.controllers.UserController.saveOrUpdateUser(userName), HandlerDef(this, "com.gamealoon.controllers.UserController", "saveOrUpdateUser", Seq(classOf[String]),"POST", """""", Routes.prefix + """user/save/$userName<[^/]+>"""))
   }
}
        

// @LINE:36
case com_gamealoon_controllers_UserController_resetPassword18(params) => {
   call(params.fromPath[String]("userName", None)) { (userName) =>
        invokeHandler(com.gamealoon.controllers.UserController.resetPassword(userName), HandlerDef(this, "com.gamealoon.controllers.UserController", "resetPassword", Seq(classOf[String]),"POST", """""", Routes.prefix + """user/password/change/$userName<[^/]+>"""))
   }
}
        

// @LINE:37
case com_gamealoon_controllers_UserController_addOrRemoveBuddy19(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.UserController.addOrRemoveBuddy(), HandlerDef(this, "com.gamealoon.controllers.UserController", "addOrRemoveBuddy", Nil,"POST", """""", Routes.prefix + """user/addOrRemoveBuddy"""))
   }
}
        

// @LINE:38
case com_gamealoon_controllers_UserController_blockOrUnblockBuddy20(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.UserController.blockOrUnblockBuddy(), HandlerDef(this, "com.gamealoon.controllers.UserController", "blockOrUnblockBuddy", Nil,"POST", """""", Routes.prefix + """user/blockOrUnblockBuddy"""))
   }
}
        

// @LINE:39
case com_gamealoon_controllers_UserController_addOrRemoveInterestedGames21(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.UserController.addOrRemoveInterestedGames(), HandlerDef(this, "com.gamealoon.controllers.UserController", "addOrRemoveInterestedGames", Nil,"POST", """""", Routes.prefix + """user/interestedOrNotInGame"""))
   }
}
        

// @LINE:40
case com_gamealoon_controllers_UserController_checkStatus22(params) => {
   call(params.fromPath[String]("userName", None), params.fromPath[String]("mediaId", None)) { (userName, mediaId) =>
        invokeHandler(com.gamealoon.controllers.UserController.checkStatus(userName, mediaId), HandlerDef(this, "com.gamealoon.controllers.UserController", "checkStatus", Seq(classOf[String], classOf[String]),"OPTIONS", """""", Routes.prefix + """user/saveOrUpdateUserAvatar/$userName<[^/]+>/$mediaId<[^/]+>"""))
   }
}
        

// @LINE:41
case com_gamealoon_controllers_UserController_saveOrUpdateUserAvatar23(params) => {
   call(params.fromPath[String]("userName", None), params.fromPath[String]("mediaId", None)) { (userName, mediaId) =>
        invokeHandler(com.gamealoon.controllers.UserController.saveOrUpdateUserAvatar(userName, mediaId), HandlerDef(this, "com.gamealoon.controllers.UserController", "saveOrUpdateUserAvatar", Seq(classOf[String], classOf[String]),"POST", """""", Routes.prefix + """user/saveOrUpdateUserAvatar/$userName<[^/]+>/$mediaId<[^/]+>"""))
   }
}
        

// @LINE:42
case com_gamealoon_controllers_UserController_validateEmail24(params) => {
   call(params.fromPath[String]("email", None)) { (email) =>
        invokeHandler(com.gamealoon.controllers.UserController.validateEmail(email), HandlerDef(this, "com.gamealoon.controllers.UserController", "validateEmail", Seq(classOf[String]),"GET", """""", Routes.prefix + """user/validateEmail/$email<[^/]+>"""))
   }
}
        

// @LINE:43
case com_gamealoon_controllers_UserController_validateUsername25(params) => {
   call(params.fromPath[String]("username", None)) { (username) =>
        invokeHandler(com.gamealoon.controllers.UserController.validateUsername(username), HandlerDef(this, "com.gamealoon.controllers.UserController", "validateUsername", Seq(classOf[String]),"GET", """""", Routes.prefix + """user/validateUsername/$username<[^/]+>"""))
   }
}
        

// @LINE:44
case com_gamealoon_controllers_UserController_getAllUsers26(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.UserController.getAllUsers(), HandlerDef(this, "com.gamealoon.controllers.UserController", "getAllUsers", Nil,"GET", """""", Routes.prefix + """users"""))
   }
}
        

// @LINE:48
case com_gamealoon_controllers_MediaController_checkStatus27(params) => {
   call(params.fromPath[String]("userName", None), params.fromPath[String]("mediaId", None), params.fromPath[String]("mediaOwnerType", None)) { (userName, mediaId, mediaOwnerType) =>
        invokeHandler(com.gamealoon.controllers.MediaController.checkStatus(userName, mediaId, mediaOwnerType), HandlerDef(this, "com.gamealoon.controllers.MediaController", "checkStatus", Seq(classOf[String], classOf[String], classOf[String]),"OPTIONS", """ Media								""", Routes.prefix + """media/uploadImage/$userName<[^/]+>/$mediaId<[^/]+>/$mediaOwnerType<[^/]+>"""))
   }
}
        

// @LINE:49
case com_gamealoon_controllers_MediaController_uploadImage28(params) => {
   call(params.fromPath[String]("userName", None), params.fromPath[String]("mediaId", None), params.fromPath[String]("mediaOwnerType", None)) { (userName, mediaId, mediaOwnerType) =>
        invokeHandler(com.gamealoon.controllers.MediaController.uploadImage(userName, mediaId, mediaOwnerType), HandlerDef(this, "com.gamealoon.controllers.MediaController", "uploadImage", Seq(classOf[String], classOf[String], classOf[String]),"POST", """""", Routes.prefix + """media/uploadImage/$userName<[^/]+>/$mediaId<[^/]+>/$mediaOwnerType<[^/]+>"""))
   }
}
        

// @LINE:50
case com_gamealoon_controllers_MediaController_fetchImages29(params) => {
   call(params.fromPath[String]("userName", None), params.fromPath[String]("timeStamp", None)) { (userName, timeStamp) =>
        invokeHandler(com.gamealoon.controllers.MediaController.fetchImages(userName, timeStamp), HandlerDef(this, "com.gamealoon.controllers.MediaController", "fetchImages", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """media/images/$userName<[^/]+>/$timeStamp<[^/]+>"""))
   }
}
        

// @LINE:54
case com_gamealoon_controllers_SearchController_searchResponse30(params) => {
   call(params.fromPath[String]("keywords", None)) { (keywords) =>
        invokeHandler(com.gamealoon.controllers.SearchController.searchResponse(keywords), HandlerDef(this, "com.gamealoon.controllers.SearchController", "searchResponse", Seq(classOf[String]),"GET", """ Search""", Routes.prefix + """search/$keywords<[^/]+>"""))
   }
}
        

// @LINE:57
case com_gamealoon_controllers_GameController_getGamesByTerm31(params) => {
   call(params.fromPath[String]("term", None)) { (term) =>
        invokeHandler(com.gamealoon.controllers.GameController.getGamesByTerm(term), HandlerDef(this, "com.gamealoon.controllers.GameController", "getGamesByTerm", Seq(classOf[String]),"GET", """ Game		""", Routes.prefix + """game/fetchGame/games/$term<[^/]+>"""))
   }
}
        

// @LINE:58
case com_gamealoon_controllers_GameController_getGame32(params) => {
   call(params.fromPath[String]("urlOrid", None), params.fromPath[String]("username", None)) { (urlOrid, username) =>
        invokeHandler(com.gamealoon.controllers.GameController.getGame(urlOrid, username), HandlerDef(this, "com.gamealoon.controllers.GameController", "getGame", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """game/$urlOrid<[^/]+>/$username<[^/]+>"""))
   }
}
        

// @LINE:59
case com_gamealoon_controllers_GameController_getAllGames33(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.GameController.getAllGames(), HandlerDef(this, "com.gamealoon.controllers.GameController", "getAllGames", Nil,"GET", """""", Routes.prefix + """games"""))
   }
}
        

// @LINE:66
case com_gamealoon_controllers_ConversationController_addComment34(params) => {
   call { 
        invokeHandler(com.gamealoon.controllers.ConversationController.addComment(), HandlerDef(this, "com.gamealoon.controllers.ConversationController", "addComment", Nil,"POST", """GET /converse/								 		com.gamealoon.controllers.ConversationController.index()""", Routes.prefix + """converse/comment/save"""))
   }
}
        

// @LINE:67
case com_gamealoon_controllers_ConversationController_getComment35(params) => {
   call(params.fromPath[String]("articleId", None), params.fromPath[String]("timeStamp", None)) { (articleId, timeStamp) =>
        invokeHandler(com.gamealoon.controllers.ConversationController.getComment(articleId, timeStamp), HandlerDef(this, "com.gamealoon.controllers.ConversationController", "getComment", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """converse/comment/get/$articleId<[^/]+>/$timeStamp<[^/]+>"""))
   }
}
        

// @LINE:73
case controllers_Assets_at36(params) => {
   call(Param[String]("path", Right("/public")), params.fromPath[String]("file", None)) { (path, file) =>
        invokeHandler(controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]),"GET", """""", Routes.prefix + """assets/$file<.+>"""))
   }
}
        
}

}
     
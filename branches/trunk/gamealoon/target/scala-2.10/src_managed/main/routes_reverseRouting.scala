// @SOURCE:E:/Gamealoon/gamealoon/conf/routes
// @HASH:f4895ae97d14f719adacc65149ef744e041a4453
// @DATE:Sat Dec 14 19:29:23 IST 2013

import Routes.{prefix => _prefix, defaultPrefix => _defaultPrefix}
import play.core._
import play.core.Router._
import play.core.j._

import play.api.mvc._
import play.libs.F

import Router.queryString


// @LINE:73
package controllers {

// @LINE:73
class ReverseAssets {
    

// @LINE:73
def at(file:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[PathBindable[String]].unbind("file", file))
}
                                                
    
}
                          
}
                  

// @LINE:67
// @LINE:66
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:54
// @LINE:50
// @LINE:49
// @LINE:48
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:41
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:7
package com.gamealoon.controllers {

// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:13
// @LINE:12
class ReverseGameController {
    

// @LINE:12
def createOrUpdateGame(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "admin/game/save")
}
                                                

// @LINE:57
def getGamesByTerm(term:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "game/fetchGame/games/" + implicitly[PathBindable[String]].unbind("term", dynamicString(term)))
}
                                                

// @LINE:13
def getNGames(timestamp:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/games/" + implicitly[PathBindable[String]].unbind("timestamp", dynamicString(timestamp)))
}
                                                

// @LINE:58
def getGame(urlOrid:String, username:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "game/" + implicitly[PathBindable[String]].unbind("urlOrid", dynamicString(urlOrid)) + "/" + implicitly[PathBindable[String]].unbind("username", dynamicString(username)))
}
                                                

// @LINE:59
def getAllGames(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "games")
}
                                                
    
}
                          

// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseArticleController {
    

// @LINE:25
def createOrUpdateCoolOrNotCoolState(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "article/votingStateUpdate")
}
                                                

// @LINE:24
def saveOrUpdateArticle(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "article/save")
}
                                                

// @LINE:22
def getArticle(username:String, titleOrId:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "article/" + implicitly[PathBindable[String]].unbind("username", dynamicString(username)) + "/" + implicitly[PathBindable[String]].unbind("titleOrId", dynamicString(titleOrId)))
}
                                                

// @LINE:23
def getNArticlesByCarouselSelectorAndCategory(carouselSelector:String, category:String, timestamp:String, mode:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "articles/" + implicitly[PathBindable[String]].unbind("carouselSelector", dynamicString(carouselSelector)) + "/" + implicitly[PathBindable[String]].unbind("category", dynamicString(category)) + "/" + implicitly[PathBindable[String]].unbind("timestamp", dynamicString(timestamp)) + "/" + implicitly[PathBindable[String]].unbind("mode", dynamicString(mode)))
}
                                                

// @LINE:26
def updateAverageTimeSpent(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "article/updateAverageTimeSpent")
}
                                                
    
}
                          

// @LINE:11
class ReverseAdminController {
    

// @LINE:11
def getAdminData(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/")
}
                                                
    
}
                          

// @LINE:17
// @LINE:16
// @LINE:7
class ReversePlatformController {
    

// @LINE:17
def getNPlatforms(timestamp:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/platforms/" + implicitly[PathBindable[String]].unbind("timestamp", dynamicString(timestamp)))
}
                                                

// @LINE:16
def createOrUpdatePlatform(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "admin/platform/save")
}
                                                

// @LINE:7
def getPlatformData(platform:String, category:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "platform/" + implicitly[PathBindable[String]].unbind("platform", dynamicString(platform)) + "/" + implicitly[PathBindable[String]].unbind("category", dynamicString(category)))
}
                                                
    
}
                          

// @LINE:50
// @LINE:49
// @LINE:48
class ReverseMediaController {
    

// @LINE:49
def uploadImage(userName:String, mediaId:String, mediaOwnerType:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "media/uploadImage/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)) + "/" + implicitly[PathBindable[String]].unbind("mediaId", dynamicString(mediaId)) + "/" + implicitly[PathBindable[String]].unbind("mediaOwnerType", dynamicString(mediaOwnerType)))
}
                                                

// @LINE:50
def fetchImages(userName:String, timeStamp:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "media/images/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)) + "/" + implicitly[PathBindable[String]].unbind("timeStamp", dynamicString(timeStamp)))
}
                                                

// @LINE:48
def checkStatus(userName:String, mediaId:String, mediaOwnerType:String): Call = {
   Call("OPTIONS", _prefix + { _defaultPrefix } + "media/uploadImage/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)) + "/" + implicitly[PathBindable[String]].unbind("mediaId", dynamicString(mediaId)) + "/" + implicitly[PathBindable[String]].unbind("mediaOwnerType", dynamicString(mediaOwnerType)))
}
                                                
    
}
                          

// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:41
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
class ReverseUserController {
    

// @LINE:34
def saveOrUpdateUserInterest(userName:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/interest/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)))
}
                                                

// @LINE:42
def validateEmail(email:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "user/validateEmail/" + implicitly[PathBindable[String]].unbind("email", dynamicString(email)))
}
                                                

// @LINE:39
def addOrRemoveInterestedGames(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/interestedOrNotInGame")
}
                                                

// @LINE:37
def addOrRemoveBuddy(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/addOrRemoveBuddy")
}
                                                

// @LINE:36
def resetPassword(userName:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/password/change/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)))
}
                                                

// @LINE:31
def registerUser(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/register")
}
                                                

// @LINE:32
def getLoggedInUser(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "login")
}
                                                

// @LINE:33
def getUser(usernameOrId:String, mode:Integer, username:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "user/" + implicitly[PathBindable[String]].unbind("usernameOrId", dynamicString(usernameOrId)) + "/" + implicitly[PathBindable[Integer]].unbind("mode", mode) + "/" + implicitly[PathBindable[String]].unbind("username", dynamicString(username)))
}
                                                

// @LINE:41
def saveOrUpdateUserAvatar(userName:String, mediaId:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/saveOrUpdateUserAvatar/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)) + "/" + implicitly[PathBindable[String]].unbind("mediaId", dynamicString(mediaId)))
}
                                                

// @LINE:40
def checkStatus(userName:String, mediaId:String): Call = {
   Call("OPTIONS", _prefix + { _defaultPrefix } + "user/saveOrUpdateUserAvatar/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)) + "/" + implicitly[PathBindable[String]].unbind("mediaId", dynamicString(mediaId)))
}
                                                

// @LINE:38
def blockOrUnblockBuddy(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/blockOrUnblockBuddy")
}
                                                

// @LINE:43
def validateUsername(username:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "user/validateUsername/" + implicitly[PathBindable[String]].unbind("username", dynamicString(username)))
}
                                                

// @LINE:44
def getAllUsers(): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "users")
}
                                                

// @LINE:35
def saveOrUpdateUser(userName:String): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "user/save/" + implicitly[PathBindable[String]].unbind("userName", dynamicString(userName)))
}
                                                
    
}
                          

// @LINE:15
// @LINE:14
class ReverseAchievementController {
    

// @LINE:15
def getNAchievements(timestamp:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "admin/achievements/" + implicitly[PathBindable[String]].unbind("timestamp", dynamicString(timestamp)))
}
                                                

// @LINE:14
def createOrUpdateAchievement(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "admin/achievement/save")
}
                                                
    
}
                          

// @LINE:54
class ReverseSearchController {
    

// @LINE:54
def searchResponse(keywords:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "search/" + implicitly[PathBindable[String]].unbind("keywords", dynamicString(keywords)))
}
                                                
    
}
                          

// @LINE:67
// @LINE:66
class ReverseConversationController {
    

// @LINE:66
def addComment(): Call = {
   Call("POST", _prefix + { _defaultPrefix } + "converse/comment/save")
}
                                                

// @LINE:67
def getComment(articleId:String, timeStamp:String): Call = {
   Call("GET", _prefix + { _defaultPrefix } + "converse/comment/get/" + implicitly[PathBindable[String]].unbind("articleId", dynamicString(articleId)) + "/" + implicitly[PathBindable[String]].unbind("timeStamp", dynamicString(timeStamp)))
}
                                                
    
}
                          
}
                  


// @LINE:73
package controllers.javascript {

// @LINE:73
class ReverseAssets {
    

// @LINE:73
def at : JavascriptReverseRoute = JavascriptReverseRoute(
   "controllers.Assets.at",
   """
      function(file) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("file", file)})
      }
   """
)
                        
    
}
              
}
        

// @LINE:67
// @LINE:66
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:54
// @LINE:50
// @LINE:49
// @LINE:48
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:41
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:7
package com.gamealoon.controllers.javascript {

// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:13
// @LINE:12
class ReverseGameController {
    

// @LINE:12
def createOrUpdateGame : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.GameController.createOrUpdateGame",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/game/save"})
      }
   """
)
                        

// @LINE:57
def getGamesByTerm : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.GameController.getGamesByTerm",
   """
      function(term) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "game/fetchGame/games/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("term", encodeURIComponent(term))})
      }
   """
)
                        

// @LINE:13
def getNGames : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.GameController.getNGames",
   """
      function(timestamp) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/games/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("timestamp", encodeURIComponent(timestamp))})
      }
   """
)
                        

// @LINE:58
def getGame : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.GameController.getGame",
   """
      function(urlOrid,username) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "game/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("urlOrid", encodeURIComponent(urlOrid)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username))})
      }
   """
)
                        

// @LINE:59
def getAllGames : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.GameController.getAllGames",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "games"})
      }
   """
)
                        
    
}
              

// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseArticleController {
    

// @LINE:25
def createOrUpdateCoolOrNotCoolState : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.ArticleController.createOrUpdateCoolOrNotCoolState",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "article/votingStateUpdate"})
      }
   """
)
                        

// @LINE:24
def saveOrUpdateArticle : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.ArticleController.saveOrUpdateArticle",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "article/save"})
      }
   """
)
                        

// @LINE:22
def getArticle : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.ArticleController.getArticle",
   """
      function(username,titleOrId) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "article/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("titleOrId", encodeURIComponent(titleOrId))})
      }
   """
)
                        

// @LINE:23
def getNArticlesByCarouselSelectorAndCategory : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.ArticleController.getNArticlesByCarouselSelectorAndCategory",
   """
      function(carouselSelector,category,timestamp,mode) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "articles/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("carouselSelector", encodeURIComponent(carouselSelector)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("category", encodeURIComponent(category)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("timestamp", encodeURIComponent(timestamp)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("mode", encodeURIComponent(mode))})
      }
   """
)
                        

// @LINE:26
def updateAverageTimeSpent : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.ArticleController.updateAverageTimeSpent",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "article/updateAverageTimeSpent"})
      }
   """
)
                        
    
}
              

// @LINE:11
class ReverseAdminController {
    

// @LINE:11
def getAdminData : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.AdminController.getAdminData",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/"})
      }
   """
)
                        
    
}
              

// @LINE:17
// @LINE:16
// @LINE:7
class ReversePlatformController {
    

// @LINE:17
def getNPlatforms : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.PlatformController.getNPlatforms",
   """
      function(timestamp) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/platforms/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("timestamp", encodeURIComponent(timestamp))})
      }
   """
)
                        

// @LINE:16
def createOrUpdatePlatform : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.PlatformController.createOrUpdatePlatform",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/platform/save"})
      }
   """
)
                        

// @LINE:7
def getPlatformData : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.PlatformController.getPlatformData",
   """
      function(platform,category) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "platform/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("platform", encodeURIComponent(platform)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("category", encodeURIComponent(category))})
      }
   """
)
                        
    
}
              

// @LINE:50
// @LINE:49
// @LINE:48
class ReverseMediaController {
    

// @LINE:49
def uploadImage : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.MediaController.uploadImage",
   """
      function(userName,mediaId,mediaOwnerType) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "media/uploadImage/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("mediaId", encodeURIComponent(mediaId)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("mediaOwnerType", encodeURIComponent(mediaOwnerType))})
      }
   """
)
                        

// @LINE:50
def fetchImages : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.MediaController.fetchImages",
   """
      function(userName,timeStamp) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "media/images/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("timeStamp", encodeURIComponent(timeStamp))})
      }
   """
)
                        

// @LINE:48
def checkStatus : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.MediaController.checkStatus",
   """
      function(userName,mediaId,mediaOwnerType) {
      return _wA({method:"OPTIONS", url:"""" + _prefix + { _defaultPrefix } + """" + "media/uploadImage/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("mediaId", encodeURIComponent(mediaId)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("mediaOwnerType", encodeURIComponent(mediaOwnerType))})
      }
   """
)
                        
    
}
              

// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:41
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
class ReverseUserController {
    

// @LINE:34
def saveOrUpdateUserInterest : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.saveOrUpdateUserInterest",
   """
      function(userName) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/interest/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName))})
      }
   """
)
                        

// @LINE:42
def validateEmail : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.validateEmail",
   """
      function(email) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "user/validateEmail/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("email", encodeURIComponent(email))})
      }
   """
)
                        

// @LINE:39
def addOrRemoveInterestedGames : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.addOrRemoveInterestedGames",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/interestedOrNotInGame"})
      }
   """
)
                        

// @LINE:37
def addOrRemoveBuddy : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.addOrRemoveBuddy",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/addOrRemoveBuddy"})
      }
   """
)
                        

// @LINE:36
def resetPassword : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.resetPassword",
   """
      function(userName) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/password/change/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName))})
      }
   """
)
                        

// @LINE:31
def registerUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.registerUser",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/register"})
      }
   """
)
                        

// @LINE:32
def getLoggedInUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.getLoggedInUser",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "login"})
      }
   """
)
                        

// @LINE:33
def getUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.getUser",
   """
      function(usernameOrId,mode,username) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "user/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("usernameOrId", encodeURIComponent(usernameOrId)) + "/" + (""" + implicitly[PathBindable[Integer]].javascriptUnbind + """)("mode", mode) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username))})
      }
   """
)
                        

// @LINE:41
def saveOrUpdateUserAvatar : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.saveOrUpdateUserAvatar",
   """
      function(userName,mediaId) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/saveOrUpdateUserAvatar/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("mediaId", encodeURIComponent(mediaId))})
      }
   """
)
                        

// @LINE:40
def checkStatus : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.checkStatus",
   """
      function(userName,mediaId) {
      return _wA({method:"OPTIONS", url:"""" + _prefix + { _defaultPrefix } + """" + "user/saveOrUpdateUserAvatar/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("mediaId", encodeURIComponent(mediaId))})
      }
   """
)
                        

// @LINE:38
def blockOrUnblockBuddy : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.blockOrUnblockBuddy",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/blockOrUnblockBuddy"})
      }
   """
)
                        

// @LINE:43
def validateUsername : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.validateUsername",
   """
      function(username) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "user/validateUsername/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("username", encodeURIComponent(username))})
      }
   """
)
                        

// @LINE:44
def getAllUsers : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.getAllUsers",
   """
      function() {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "users"})
      }
   """
)
                        

// @LINE:35
def saveOrUpdateUser : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.UserController.saveOrUpdateUser",
   """
      function(userName) {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "user/save/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("userName", encodeURIComponent(userName))})
      }
   """
)
                        
    
}
              

// @LINE:15
// @LINE:14
class ReverseAchievementController {
    

// @LINE:15
def getNAchievements : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.AchievementController.getNAchievements",
   """
      function(timestamp) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/achievements/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("timestamp", encodeURIComponent(timestamp))})
      }
   """
)
                        

// @LINE:14
def createOrUpdateAchievement : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.AchievementController.createOrUpdateAchievement",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "admin/achievement/save"})
      }
   """
)
                        
    
}
              

// @LINE:54
class ReverseSearchController {
    

// @LINE:54
def searchResponse : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.SearchController.searchResponse",
   """
      function(keywords) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "search/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("keywords", encodeURIComponent(keywords))})
      }
   """
)
                        
    
}
              

// @LINE:67
// @LINE:66
class ReverseConversationController {
    

// @LINE:66
def addComment : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.ConversationController.addComment",
   """
      function() {
      return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "converse/comment/save"})
      }
   """
)
                        

// @LINE:67
def getComment : JavascriptReverseRoute = JavascriptReverseRoute(
   "com.gamealoon.controllers.ConversationController.getComment",
   """
      function(articleId,timeStamp) {
      return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "converse/comment/get/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("articleId", encodeURIComponent(articleId)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("timeStamp", encodeURIComponent(timeStamp))})
      }
   """
)
                        
    
}
              
}
        


// @LINE:73
package controllers.ref {


// @LINE:73
class ReverseAssets {
    

// @LINE:73
def at(path:String, file:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   controllers.Assets.at(path, file), HandlerDef(this, "controllers.Assets", "at", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """assets/$file<.+>""")
)
                      
    
}
                          
}
        

// @LINE:67
// @LINE:66
// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:54
// @LINE:50
// @LINE:49
// @LINE:48
// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:41
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
// @LINE:17
// @LINE:16
// @LINE:15
// @LINE:14
// @LINE:13
// @LINE:12
// @LINE:11
// @LINE:7
package com.gamealoon.controllers.ref {


// @LINE:59
// @LINE:58
// @LINE:57
// @LINE:13
// @LINE:12
class ReverseGameController {
    

// @LINE:12
def createOrUpdateGame(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.GameController.createOrUpdateGame(), HandlerDef(this, "com.gamealoon.controllers.GameController", "createOrUpdateGame", Seq(), "POST", """""", _prefix + """admin/game/save""")
)
                      

// @LINE:57
def getGamesByTerm(term:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.GameController.getGamesByTerm(term), HandlerDef(this, "com.gamealoon.controllers.GameController", "getGamesByTerm", Seq(classOf[String]), "GET", """ Game		""", _prefix + """game/fetchGame/games/$term<[^/]+>""")
)
                      

// @LINE:13
def getNGames(timestamp:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.GameController.getNGames(timestamp), HandlerDef(this, "com.gamealoon.controllers.GameController", "getNGames", Seq(classOf[String]), "GET", """""", _prefix + """admin/games/$timestamp<[^/]+>""")
)
                      

// @LINE:58
def getGame(urlOrid:String, username:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.GameController.getGame(urlOrid, username), HandlerDef(this, "com.gamealoon.controllers.GameController", "getGame", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """game/$urlOrid<[^/]+>/$username<[^/]+>""")
)
                      

// @LINE:59
def getAllGames(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.GameController.getAllGames(), HandlerDef(this, "com.gamealoon.controllers.GameController", "getAllGames", Seq(), "GET", """""", _prefix + """games""")
)
                      
    
}
                          

// @LINE:26
// @LINE:25
// @LINE:24
// @LINE:23
// @LINE:22
class ReverseArticleController {
    

// @LINE:25
def createOrUpdateCoolOrNotCoolState(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.ArticleController.createOrUpdateCoolOrNotCoolState(), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "createOrUpdateCoolOrNotCoolState", Seq(), "POST", """""", _prefix + """article/votingStateUpdate""")
)
                      

// @LINE:24
def saveOrUpdateArticle(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.ArticleController.saveOrUpdateArticle(), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "saveOrUpdateArticle", Seq(), "POST", """""", _prefix + """article/save""")
)
                      

// @LINE:22
def getArticle(username:String, titleOrId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.ArticleController.getArticle(username, titleOrId), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "getArticle", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """article/$username<[^/]+>/$titleOrId<[^/]+>""")
)
                      

// @LINE:23
def getNArticlesByCarouselSelectorAndCategory(carouselSelector:String, category:String, timestamp:String, mode:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.ArticleController.getNArticlesByCarouselSelectorAndCategory(carouselSelector, category, timestamp, mode), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "getNArticlesByCarouselSelectorAndCategory", Seq(classOf[String], classOf[String], classOf[String], classOf[String]), "GET", """""", _prefix + """articles/$carouselSelector<[^/]+>/$category<[^/]+>/$timestamp<[^/]+>/$mode<[^/]+>""")
)
                      

// @LINE:26
def updateAverageTimeSpent(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.ArticleController.updateAverageTimeSpent(), HandlerDef(this, "com.gamealoon.controllers.ArticleController", "updateAverageTimeSpent", Seq(), "POST", """""", _prefix + """article/updateAverageTimeSpent""")
)
                      
    
}
                          

// @LINE:11
class ReverseAdminController {
    

// @LINE:11
def getAdminData(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.AdminController.getAdminData(), HandlerDef(this, "com.gamealoon.controllers.AdminController", "getAdminData", Seq(), "GET", """""", _prefix + """admin/""")
)
                      
    
}
                          

// @LINE:17
// @LINE:16
// @LINE:7
class ReversePlatformController {
    

// @LINE:17
def getNPlatforms(timestamp:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.PlatformController.getNPlatforms(timestamp), HandlerDef(this, "com.gamealoon.controllers.PlatformController", "getNPlatforms", Seq(classOf[String]), "GET", """""", _prefix + """admin/platforms/$timestamp<[^/]+>""")
)
                      

// @LINE:16
def createOrUpdatePlatform(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.PlatformController.createOrUpdatePlatform(), HandlerDef(this, "com.gamealoon.controllers.PlatformController", "createOrUpdatePlatform", Seq(), "POST", """""", _prefix + """admin/platform/save""")
)
                      

// @LINE:7
def getPlatformData(platform:String, category:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.PlatformController.getPlatformData(platform, category), HandlerDef(this, "com.gamealoon.controllers.PlatformController", "getPlatformData", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """platform/$platform<[^/]+>/$category<[^/]+>""")
)
                      
    
}
                          

// @LINE:50
// @LINE:49
// @LINE:48
class ReverseMediaController {
    

// @LINE:49
def uploadImage(userName:String, mediaId:String, mediaOwnerType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.MediaController.uploadImage(userName, mediaId, mediaOwnerType), HandlerDef(this, "com.gamealoon.controllers.MediaController", "uploadImage", Seq(classOf[String], classOf[String], classOf[String]), "POST", """""", _prefix + """media/uploadImage/$userName<[^/]+>/$mediaId<[^/]+>/$mediaOwnerType<[^/]+>""")
)
                      

// @LINE:50
def fetchImages(userName:String, timeStamp:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.MediaController.fetchImages(userName, timeStamp), HandlerDef(this, "com.gamealoon.controllers.MediaController", "fetchImages", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """media/images/$userName<[^/]+>/$timeStamp<[^/]+>""")
)
                      

// @LINE:48
def checkStatus(userName:String, mediaId:String, mediaOwnerType:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.MediaController.checkStatus(userName, mediaId, mediaOwnerType), HandlerDef(this, "com.gamealoon.controllers.MediaController", "checkStatus", Seq(classOf[String], classOf[String], classOf[String]), "OPTIONS", """ Media								""", _prefix + """media/uploadImage/$userName<[^/]+>/$mediaId<[^/]+>/$mediaOwnerType<[^/]+>""")
)
                      
    
}
                          

// @LINE:44
// @LINE:43
// @LINE:42
// @LINE:41
// @LINE:40
// @LINE:39
// @LINE:38
// @LINE:37
// @LINE:36
// @LINE:35
// @LINE:34
// @LINE:33
// @LINE:32
// @LINE:31
class ReverseUserController {
    

// @LINE:34
def saveOrUpdateUserInterest(userName:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.saveOrUpdateUserInterest(userName), HandlerDef(this, "com.gamealoon.controllers.UserController", "saveOrUpdateUserInterest", Seq(classOf[String]), "POST", """""", _prefix + """user/interest/$userName<[^/]+>""")
)
                      

// @LINE:42
def validateEmail(email:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.validateEmail(email), HandlerDef(this, "com.gamealoon.controllers.UserController", "validateEmail", Seq(classOf[String]), "GET", """""", _prefix + """user/validateEmail/$email<[^/]+>""")
)
                      

// @LINE:39
def addOrRemoveInterestedGames(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.addOrRemoveInterestedGames(), HandlerDef(this, "com.gamealoon.controllers.UserController", "addOrRemoveInterestedGames", Seq(), "POST", """""", _prefix + """user/interestedOrNotInGame""")
)
                      

// @LINE:37
def addOrRemoveBuddy(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.addOrRemoveBuddy(), HandlerDef(this, "com.gamealoon.controllers.UserController", "addOrRemoveBuddy", Seq(), "POST", """""", _prefix + """user/addOrRemoveBuddy""")
)
                      

// @LINE:36
def resetPassword(userName:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.resetPassword(userName), HandlerDef(this, "com.gamealoon.controllers.UserController", "resetPassword", Seq(classOf[String]), "POST", """""", _prefix + """user/password/change/$userName<[^/]+>""")
)
                      

// @LINE:31
def registerUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.registerUser(), HandlerDef(this, "com.gamealoon.controllers.UserController", "registerUser", Seq(), "POST", """""", _prefix + """user/register""")
)
                      

// @LINE:32
def getLoggedInUser(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.getLoggedInUser(), HandlerDef(this, "com.gamealoon.controllers.UserController", "getLoggedInUser", Seq(), "POST", """""", _prefix + """login""")
)
                      

// @LINE:33
def getUser(usernameOrId:String, mode:Integer, username:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.getUser(usernameOrId, mode, username), HandlerDef(this, "com.gamealoon.controllers.UserController", "getUser", Seq(classOf[String], classOf[Integer], classOf[String]), "GET", """""", _prefix + """user/$usernameOrId<[^/]+>/$mode<[^/]+>/$username<[^/]+>""")
)
                      

// @LINE:41
def saveOrUpdateUserAvatar(userName:String, mediaId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.saveOrUpdateUserAvatar(userName, mediaId), HandlerDef(this, "com.gamealoon.controllers.UserController", "saveOrUpdateUserAvatar", Seq(classOf[String], classOf[String]), "POST", """""", _prefix + """user/saveOrUpdateUserAvatar/$userName<[^/]+>/$mediaId<[^/]+>""")
)
                      

// @LINE:40
def checkStatus(userName:String, mediaId:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.checkStatus(userName, mediaId), HandlerDef(this, "com.gamealoon.controllers.UserController", "checkStatus", Seq(classOf[String], classOf[String]), "OPTIONS", """""", _prefix + """user/saveOrUpdateUserAvatar/$userName<[^/]+>/$mediaId<[^/]+>""")
)
                      

// @LINE:38
def blockOrUnblockBuddy(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.blockOrUnblockBuddy(), HandlerDef(this, "com.gamealoon.controllers.UserController", "blockOrUnblockBuddy", Seq(), "POST", """""", _prefix + """user/blockOrUnblockBuddy""")
)
                      

// @LINE:43
def validateUsername(username:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.validateUsername(username), HandlerDef(this, "com.gamealoon.controllers.UserController", "validateUsername", Seq(classOf[String]), "GET", """""", _prefix + """user/validateUsername/$username<[^/]+>""")
)
                      

// @LINE:44
def getAllUsers(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.getAllUsers(), HandlerDef(this, "com.gamealoon.controllers.UserController", "getAllUsers", Seq(), "GET", """""", _prefix + """users""")
)
                      

// @LINE:35
def saveOrUpdateUser(userName:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.UserController.saveOrUpdateUser(userName), HandlerDef(this, "com.gamealoon.controllers.UserController", "saveOrUpdateUser", Seq(classOf[String]), "POST", """""", _prefix + """user/save/$userName<[^/]+>""")
)
                      
    
}
                          

// @LINE:15
// @LINE:14
class ReverseAchievementController {
    

// @LINE:15
def getNAchievements(timestamp:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.AchievementController.getNAchievements(timestamp), HandlerDef(this, "com.gamealoon.controllers.AchievementController", "getNAchievements", Seq(classOf[String]), "GET", """""", _prefix + """admin/achievements/$timestamp<[^/]+>""")
)
                      

// @LINE:14
def createOrUpdateAchievement(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.AchievementController.createOrUpdateAchievement(), HandlerDef(this, "com.gamealoon.controllers.AchievementController", "createOrUpdateAchievement", Seq(), "POST", """""", _prefix + """admin/achievement/save""")
)
                      
    
}
                          

// @LINE:54
class ReverseSearchController {
    

// @LINE:54
def searchResponse(keywords:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.SearchController.searchResponse(keywords), HandlerDef(this, "com.gamealoon.controllers.SearchController", "searchResponse", Seq(classOf[String]), "GET", """ Search""", _prefix + """search/$keywords<[^/]+>""")
)
                      
    
}
                          

// @LINE:67
// @LINE:66
class ReverseConversationController {
    

// @LINE:66
def addComment(): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.ConversationController.addComment(), HandlerDef(this, "com.gamealoon.controllers.ConversationController", "addComment", Seq(), "POST", """GET /converse/								 		com.gamealoon.controllers.ConversationController.index()""", _prefix + """converse/comment/save""")
)
                      

// @LINE:67
def getComment(articleId:String, timeStamp:String): play.api.mvc.HandlerRef[_] = new play.api.mvc.HandlerRef(
   com.gamealoon.controllers.ConversationController.getComment(articleId, timeStamp), HandlerDef(this, "com.gamealoon.controllers.ConversationController", "getComment", Seq(classOf[String], classOf[String]), "GET", """""", _prefix + """converse/comment/get/$articleId<[^/]+>/$timeStamp<[^/]+>""")
)
                      
    
}
                          
}
        
    
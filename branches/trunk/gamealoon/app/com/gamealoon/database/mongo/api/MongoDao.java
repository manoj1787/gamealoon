package com.gamealoon.database.mongo.api;

public interface MongoDao {
	
	/**
	 * @param game
	 */
	void save(Object object);
}
